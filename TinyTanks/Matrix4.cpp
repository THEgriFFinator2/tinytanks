//////////////////////////////////////
//File:		Matrix4.ccp				
//Author:	Matt Griffin				
//Brief:	Matrix4 Code File	    
//////////////////////////////////////

#include "Vector4.h"
#include "Matrix4.h"
#include "MathsUtilities.h"
#include <cmath>

//Contructors for Matrix4
Matrix4::Matrix4()
{}

Matrix4::Matrix4(const float& a_c_rfm11, const float& a_c_rfm12, const float& a_c_rfm13, const float& a_c_rfm14, const float& a_c_rfm21, const float& a_c_rfm22, const float& a_c_rfm23, const float& a_c_rfm24, const float& a_c_rfm31, const float& a_c_rfm32, const float& a_c_rfm33, const float& a_c_rfm34, const float& a_c_rfm41, const float& a_c_rfm42, const float& a_c_rfm43, const float& a_c_rfm44)
{
	m_11 = a_c_rfm11;
	m_12 = a_c_rfm12;
	m_13 = a_c_rfm13;
	m_14 = a_c_rfm14;

	m_21 = a_c_rfm21;
	m_22 = a_c_rfm22;
	m_23 = a_c_rfm23;
	m_24 = a_c_rfm24;

	m_31 = a_c_rfm31;
	m_32 = a_c_rfm32;
	m_33 = a_c_rfm33;
	m_34 = a_c_rfm34;

	m_41 = a_c_rfm41;
	m_42 = a_c_rfm42;
	m_43 = a_c_rfm43;
	m_44 = a_c_rfm44;
}

Matrix4::Matrix4(const Vector4& a_c_rV4_1, const Vector4& a_c_rV4_2, const Vector4& a_c_rV4_3, const Vector4& a_c_rV4_4)
{
	xAxis.fx = a_c_rV4_1.fx;
	xAxis.fy = a_c_rV4_1.fy;
	xAxis.fz = a_c_rV4_1.fz;
	xAxis.fw = a_c_rV4_1.fw;

	yAxis.fx = a_c_rV4_2.fx;
	yAxis.fy = a_c_rV4_2.fy;
	yAxis.fz = a_c_rV4_2.fz;
	zAxis.fw = a_c_rV4_2.fw;

	zAxis.fx = a_c_rV4_3.fx;
	zAxis.fy = a_c_rV4_3.fy;
	zAxis.fz = a_c_rV4_3.fz;
	zAxis.fw = a_c_rV4_3.fw;

	wAxis.fx = a_c_rV4_4.fx;
	wAxis.fy = a_c_rV4_4.fy;
	wAxis.fz = a_c_rV4_4.fz;
	wAxis.fw = a_c_rV4_4.fw;
}

Matrix4::Matrix4(const Matrix4& a_c_rM4)
{
	m_11 = a_c_rM4.m_11;
	m_12 = a_c_rM4.m_12;
	m_13 = a_c_rM4.m_13;
	m_14 = a_c_rM4.m_14;

	m_21 = a_c_rM4.m_21;
	m_22 = a_c_rM4.m_22;
	m_23 = a_c_rM4.m_23;
	m_24 = a_c_rM4.m_24;

	m_31 = a_c_rM4.m_31;
	m_32 = a_c_rM4.m_32;
	m_33 = a_c_rM4.m_33;
	m_34 = a_c_rM4.m_34;

	m_41 = a_c_rM4.m_41;
	m_42 = a_c_rM4.m_42;
	m_43 = a_c_rM4.m_43;
	m_44 = a_c_rM4.m_44;
}

//Destructor for Matrix4
Matrix4::~Matrix4()
{}

//Functions for Matrix4
bool Matrix4::invert()
{
	const float fDet = determinant3();
	if (fDet != 0.0f)
	{
		const float fInvDet = Recipf(fDet);
		Matrix4 temp = *this;
		m_11 = (temp.m_22 * temp.m_33 - temp.m_23 * temp.m_32) * fInvDet;
		m_12 = (temp.m_13 * temp.m_32 - temp.m_12 * temp.m_33) * fInvDet;
		m_13 = (temp.m_12 * temp.m_23 - temp.m_13 * temp.m_22) * fInvDet;
		m_14 = 0.0f;

		m_21 = (temp.m_23 * temp.m_31 - temp.m_21 * temp.m_33) * fInvDet;
		m_22 = (temp.m_11 * temp.m_33 - temp.m_13 * temp.m_31) * fInvDet;
		m_23 = (temp.m_13 * temp.m_21 - temp.m_11 * temp.m_23) * fInvDet;
		m_24 = 0.0f;

		m_31 = (temp.m_21 * temp.m_32 - temp.m_22 * temp.m_31) * fInvDet;
		m_32 = (temp.m_12 * temp.m_31 - temp.m_11 * temp.m_32) * fInvDet;
		m_33 = (temp.m_11 * temp.m_22 - temp.m_12 * temp.m_21) * fInvDet;
		m_34 = 0.0f;

		m_41 = (temp.m_21 * (temp.m_33 * temp.m_42 - temp.m_32 * temp.m_43) +
			temp.m_22 * (temp.m_31 * temp.m_43 - temp.m_33 * temp.m_41) +
			temp.m_23 * (temp.m_32 * temp.m_41 - temp.m_31 * temp.m_42)) *fInvDet;
		m_41 = (temp.m_11 * (temp.m_32 * temp.m_43 - temp.m_33 * temp.m_42) +
			temp.m_12 * (temp.m_33 * temp.m_41 - temp.m_31 * temp.m_43) +
			temp.m_13 * (temp.m_31 * temp.m_42 - temp.m_32 * temp.m_41)) *fInvDet;
		m_41 = (temp.m_11 * (temp.m_23 * temp.m_42 - temp.m_22 * temp.m_43) +
			temp.m_12 * (temp.m_21 * temp.m_43 - temp.m_23 * temp.m_41) +
			temp.m_13 * (temp.m_22 * temp.m_41 - temp.m_21 * temp.m_42)) *fInvDet;
		m_44 = 0.0f;

		return true;
	}
	else
	{
		return false;
	}
}

void Matrix4::scaleX(const float& a_c_rfScalar)
{
	xAxis *= a_c_rfScalar;
}

void Matrix4::scaleY(const float& a_c_rfScalar)
{
	yAxis *= a_c_rfScalar;
}

void Matrix4::scaleZ(const float& a_c_rfScalar)
{
	zAxis *= a_c_rfScalar;
}

void Matrix4::scale(const float& a_c_rfScalar)
{
	scaleX(a_c_rfScalar);
	scaleY(a_c_rfScalar);
	scaleZ(a_c_rfScalar);
}

void Matrix4::scale(const float& a_c_rfScalarX, const float& a_c_rfScalarY, const float& a_c_rfScalarZ)
{
	scaleX(a_c_rfScalarX);
	scaleY(a_c_rfScalarY);
	scaleZ(a_c_rfScalarZ);
}

void Matrix4::transpose()
{
	float fTempm_12 = m_21, fTempm_13 = m_31, fTempm_14 = m_41;
	float fTempm_23 = m_32, fTempm_24 = m_42;
	float fTempm_34 = m_43;

	float fTempm_21 = m_12, fTempm_31 = m_13, fTempm_41 = m_14;
	float fTempm_32 = m_23, fTempm_42 = m_24;
	float fTempm_43 = m_34;

	m_12 = fTempm_12;
	m_13 = fTempm_13;                       //Check area for clarification
	m_23 = fTempm_23;

	m_21 = fTempm_21;
	m_31 = fTempm_31;
	m_32 = fTempm_32;
}

void Matrix4::setToIdentity()
{
	m_11 = 1;
	m_12 = 0;
	m_13 = 0;
	m_14 = 0;

	m_21 = 0;
	m_22 = 1;
	m_23 = 0;
	m_24 = 0;

	m_31 = 0;
	m_32 = 0;
	m_33 = 1;
	m_34 = 0;

	m_41 = 0;
	m_42 = 0;
	m_43 = 0;
	m_44 = 1;
}

void Matrix4::setToZero()
{
	m_11 = 0;
	m_12 = 0;
	m_13 = 0;
	m_14 = 0;

	m_21 = 0;
	m_22 = 0;
	m_23 = 0;
	m_24 = 0;

	m_31 = 0;
	m_32 = 0;
	m_33 = 0;
	m_34 = 0;

	m_41 = 0;
	m_42 = 0;
	m_43 = 0;
	m_44 = 0;
}

void Matrix4::rotateX(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	const float si = sinf(fRad);
	const float co = cosf(fRad);
	m_11 = 1.0f; m_12 = 0.0f; m_13 = 0.0f; m_14 = 0.0f;
	m_21 = 0.0f; m_22 = co; m_23 = si; m_24 = 0.0f;
	m_31 = 0.0f; m_32 = -si; m_33 = co; m_34 = 0.0f;
	m_41 = 0.0f; m_42 = 0.0f; m_43 = 0.0f; m_44 = 1.0f;
}

void Matrix4::rotateY(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	const float si = sinf(fRad);
	const float co = cosf(fRad);
	m_11 = co; m_12 = 0.0f; m_13 = -si; m_14 = 0.0f;
	m_21 = 0.0f; m_22 = 1.0f; m_23 = 0.0f; m_24 = 0.0f;
	m_31 = si; m_32 = 0.0f; m_33 = co; m_34 = 0.0f;
	m_41 = 0.0f; m_42 = 0.0f; m_43 = 0.0f; m_44 = 1.0f;
}

void Matrix4::rotateZ(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	const float si = sinf(fRad);
	const float co = cosf(fRad);
	m_11 = co; m_12 = si; m_13 = 0.0f; m_14 = 0.0f;
	m_21 = -si; m_22 = co; m_23 = 0.0f; m_24 = 0.0f;
	m_31 = 0.0f; m_32 = 0.0f; m_33 = 1.0f; m_34 = 0.0f;
	m_41 = 0.0f; m_42 = 0.0f; m_43 = 0.0f; m_44 = 1.0f;
}

//Operator Overloads - Mathematical
Matrix4 Matrix4::operator+(const float& a_c_rfScalar)
{
	Matrix4 temp = *this;

	temp.m_11 += a_c_rfScalar;
	temp.m_12 += a_c_rfScalar;
	temp.m_13 += a_c_rfScalar;
	temp.m_14 += a_c_rfScalar;

	temp.m_21 += a_c_rfScalar;
	temp.m_22 += a_c_rfScalar;
	temp.m_23 += a_c_rfScalar;
	temp.m_24 += a_c_rfScalar;

	temp.m_31 += a_c_rfScalar;
	temp.m_32 += a_c_rfScalar;
	temp.m_33 += a_c_rfScalar;
	temp.m_34 += a_c_rfScalar;

	temp.m_41 += a_c_rfScalar;
	temp.m_42 += a_c_rfScalar;
	temp.m_43 += a_c_rfScalar;
	temp.m_44 += a_c_rfScalar;

	return temp;
}

Matrix4 Matrix4::operator+(const Vector3& a_c_rV3)
{
	Matrix4 temp = *this;



	return temp;
}

Matrix4 Matrix4::operator+(const Vector4& a_c_rV4)
{
	Matrix4 temp = *this;



	return temp;
}

Matrix4 Matrix4::operator+(const Matrix4& a_c_rM4)
{
	Matrix4 temp = *this;

	temp.m_11 += a_c_rM4.m_11;
	temp.m_12 += a_c_rM4.m_12;
	temp.m_13 += a_c_rM4.m_13;
	temp.m_14 += a_c_rM4.m_14;

	temp.m_21 += a_c_rM4.m_21;
	temp.m_22 += a_c_rM4.m_22;
	temp.m_23 += a_c_rM4.m_23;
	temp.m_24 += a_c_rM4.m_24;

	temp.m_31 += a_c_rM4.m_31;
	temp.m_32 += a_c_rM4.m_32;
	temp.m_33 += a_c_rM4.m_33;
	temp.m_34 += a_c_rM4.m_34;

	temp.m_41 += a_c_rM4.m_41;
	temp.m_42 += a_c_rM4.m_42;
	temp.m_43 += a_c_rM4.m_43;
	temp.m_44 += a_c_rM4.m_44;

	return temp;
}

Matrix4 Matrix4::operator-(const float& a_c_rfScalar)
{
	Matrix4 temp = *this;

	temp.m_11 -= a_c_rfScalar;
	temp.m_12 -= a_c_rfScalar;
	temp.m_13 -= a_c_rfScalar;
	temp.m_14 -= a_c_rfScalar;

	temp.m_21 -= a_c_rfScalar;
	temp.m_22 -= a_c_rfScalar;
	temp.m_23 -= a_c_rfScalar;
	temp.m_24 -= a_c_rfScalar;

	temp.m_31 -= a_c_rfScalar;
	temp.m_32 -= a_c_rfScalar;
	temp.m_33 -= a_c_rfScalar;
	temp.m_34 -= a_c_rfScalar;

	temp.m_41 -= a_c_rfScalar;
	temp.m_42 -= a_c_rfScalar;
	temp.m_43 -= a_c_rfScalar;
	temp.m_44 -= a_c_rfScalar;

	return temp;
}

Matrix4 Matrix4::operator-(const Matrix4& a_c_rM4)
{
	Matrix4 temp = *this;

	temp.m_11 -= a_c_rM4.m_11;
	temp.m_12 -= a_c_rM4.m_12;
	temp.m_13 -= a_c_rM4.m_13;
	temp.m_14 -= a_c_rM4.m_14;

	temp.m_21 -= a_c_rM4.m_21;
	temp.m_22 -= a_c_rM4.m_22;
	temp.m_23 -= a_c_rM4.m_23;
	temp.m_24 -= a_c_rM4.m_24;

	temp.m_31 -= a_c_rM4.m_31;
	temp.m_32 -= a_c_rM4.m_32;
	temp.m_33 -= a_c_rM4.m_33;
	temp.m_34 -= a_c_rM4.m_34;

	temp.m_41 -= a_c_rM4.m_41;
	temp.m_42 -= a_c_rM4.m_42;
	temp.m_43 -= a_c_rM4.m_43;
	temp.m_44 -= a_c_rM4.m_44;

	return temp;
}

Matrix4 Matrix4::operator*(const float& a_c_rfScalar)
{
	Matrix4 temp = *this;

	temp.m_11 *= a_c_rfScalar;
	temp.m_12 *= a_c_rfScalar;
	temp.m_13 *= a_c_rfScalar;
	temp.m_14 *= a_c_rfScalar;

	temp.m_21 *= a_c_rfScalar;
	temp.m_22 *= a_c_rfScalar;
	temp.m_23 *= a_c_rfScalar;
	temp.m_24 *= a_c_rfScalar;

	temp.m_31 *= a_c_rfScalar;
	temp.m_32 *= a_c_rfScalar;
	temp.m_33 *= a_c_rfScalar;
	temp.m_34 *= a_c_rfScalar;

	temp.m_41 *= a_c_rfScalar;
	temp.m_42 *= a_c_rfScalar;
	temp.m_43 *= a_c_rfScalar;
	temp.m_44 *= a_c_rfScalar;

	return temp;
}

Matrix4 Matrix4::operator*(const Matrix4& a_c_rM4)
{
	Matrix4 temp = *this;

	temp.m_11 *= a_c_rM4.m_11;
	temp.m_12 *= a_c_rM4.m_12;
	temp.m_13 *= a_c_rM4.m_13;
	temp.m_14 *= a_c_rM4.m_14;

	temp.m_21 *= a_c_rM4.m_21;
	temp.m_22 *= a_c_rM4.m_22;
	temp.m_23 *= a_c_rM4.m_23;
	temp.m_24 *= a_c_rM4.m_24;

	temp.m_31 *= a_c_rM4.m_31;
	temp.m_32 *= a_c_rM4.m_32;
	temp.m_33 *= a_c_rM4.m_33;
	temp.m_34 *= a_c_rM4.m_34;

	temp.m_41 *= a_c_rM4.m_41;
	temp.m_42 *= a_c_rM4.m_42;
	temp.m_43 *= a_c_rM4.m_43;
	temp.m_44 *= a_c_rM4.m_44;

	return temp;
}


void Matrix4::operator+=(const float& a_c_rfScalar)
{
	m_11 += a_c_rfScalar;
	m_12 += a_c_rfScalar;
	m_13 += a_c_rfScalar;
	m_14 += a_c_rfScalar;

	m_21 += a_c_rfScalar;
	m_22 += a_c_rfScalar;
	m_23 += a_c_rfScalar;
	m_34 += a_c_rfScalar;

	m_31 += a_c_rfScalar;
	m_32 += a_c_rfScalar;
	m_33 += a_c_rfScalar;
	m_34 += a_c_rfScalar;

	m_41 += a_c_rfScalar;
	m_42 += a_c_rfScalar;
	m_43 += a_c_rfScalar;
	m_44 += a_c_rfScalar;
}

void Matrix4::operator+=(const Matrix4& a_c_rM4)
{
	m_11 += a_c_rM4.m_11;
	m_12 += a_c_rM4.m_12;
	m_13 += a_c_rM4.m_13;
	m_14 += a_c_rM4.m_14;

	m_21 += a_c_rM4.m_21;
	m_22 += a_c_rM4.m_22;
	m_23 += a_c_rM4.m_23;
	m_24 += a_c_rM4.m_24;

	m_31 += a_c_rM4.m_31;
	m_32 += a_c_rM4.m_32;
	m_33 += a_c_rM4.m_33;
	m_34 += a_c_rM4.m_34;

	m_41 += a_c_rM4.m_41;
	m_42 += a_c_rM4.m_42;
	m_43 += a_c_rM4.m_43;
	m_44 += a_c_rM4.m_44;
}

void Matrix4::operator-=(const float& a_c_rfScalar)
{
	m_11 -= a_c_rfScalar;
	m_12 -= a_c_rfScalar;
	m_13 -= a_c_rfScalar;
	m_14 -= a_c_rfScalar;

	m_21 -= a_c_rfScalar;
	m_22 -= a_c_rfScalar;
	m_23 -= a_c_rfScalar;
	m_34 -= a_c_rfScalar;

	m_31 -= a_c_rfScalar;
	m_32 -= a_c_rfScalar;
	m_33 -= a_c_rfScalar;
	m_34 -= a_c_rfScalar;

	m_41 -= a_c_rfScalar;
	m_42 -= a_c_rfScalar;
	m_43 -= a_c_rfScalar;
	m_44 -= a_c_rfScalar;
}

void Matrix4::operator-=(const Matrix4& a_c_rM4)
{
	m_11 -= a_c_rM4.m_11;
	m_12 -= a_c_rM4.m_12;
	m_13 -= a_c_rM4.m_13;
	m_14 -= a_c_rM4.m_14;

	m_21 -= a_c_rM4.m_21;
	m_22 -= a_c_rM4.m_22;
	m_23 -= a_c_rM4.m_23;
	m_24 -= a_c_rM4.m_24;

	m_31 -= a_c_rM4.m_31;
	m_32 -= a_c_rM4.m_32;
	m_33 -= a_c_rM4.m_33;
	m_34 -= a_c_rM4.m_34;

	m_41 -= a_c_rM4.m_41;
	m_42 -= a_c_rM4.m_42;
	m_43 -= a_c_rM4.m_43;
	m_44 -= a_c_rM4.m_44;
}

void Matrix4::operator*=(const float& a_c_rfScalar)
{
	m_11 *= a_c_rfScalar;
	m_12 *= a_c_rfScalar;
	m_13 *= a_c_rfScalar;
	m_14 *= a_c_rfScalar;

	m_21 *= a_c_rfScalar;
	m_22 *= a_c_rfScalar;
	m_23 *= a_c_rfScalar;
	m_34 *= a_c_rfScalar;

	m_31 *= a_c_rfScalar;
	m_32 *= a_c_rfScalar;
	m_33 *= a_c_rfScalar;
	m_34 *= a_c_rfScalar;

	m_41 *= a_c_rfScalar;
	m_42 *= a_c_rfScalar;
	m_43 *= a_c_rfScalar;
	m_44 *= a_c_rfScalar;
}

void Matrix4::operator*=(const Matrix4& a_c_rM4)
{
	m_11 *= a_c_rM4.m_11;
	m_12 *= a_c_rM4.m_12;
	m_13 *= a_c_rM4.m_13;
	m_14 *= a_c_rM4.m_14;

	m_21 *= a_c_rM4.m_21;
	m_22 *= a_c_rM4.m_22;
	m_23 *= a_c_rM4.m_23;
	m_24 *= a_c_rM4.m_24;

	m_31 *= a_c_rM4.m_31;
	m_32 *= a_c_rM4.m_32;
	m_33 *= a_c_rM4.m_33;
	m_34 *= a_c_rM4.m_34;

	m_41 *= a_c_rM4.m_41;
	m_42 *= a_c_rM4.m_42;
	m_43 *= a_c_rM4.m_43;
	m_44 *= a_c_rM4.m_44;
}

//Zero and Identity
const Matrix4 Matrix4::ZERO(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
const Matrix4 Matrix4::IDENTITY(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);