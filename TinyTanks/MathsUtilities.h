//////////////////////////////////////
//File:		MathsUltilties.h		
//Author:	Matt Griffin				
//Brief:	Header File for Maths	
//////////////////////////////////////

void DegreesToRadians(float& a_rfDegrees);
void RadiansToDegrees(float& a_rfRadians);