//////////////////////////////////////
//File:		Vector2.ccp				
//Author:	Matt Griffin				
//Brief:	Vector3 Code File	    
//////////////////////////////////////

#include "Vector2.h"
#include "MathsUtilities.h"
#include <cmath>

//Constructors for Vector 2
Vector2::Vector2()
{}

Vector2::Vector2(const float& a_c_rfx, const float& a_c_rfy) :
	fx(a_c_rfx), fy(a_c_rfy)
{}

Vector2::Vector2(const Vector2& a_c_rVector2) :
	fx(a_c_rVector2.fx), fy(a_c_rVector2.fy)
{}

//Destructor for Vector 2
Vector2::~Vector2()
{}

//Functions for Vector 2

//This function returns the dot product of two objects of Vector2
float Vector2::dot(const Vector2& a_c_rVector2)
{
	return (fx * a_c_rVector2.fx) + (fy * a_c_rVector2.fy);
}

//This function finds the direction and will return the appropriate perpendicular Vector2*****
Vector2 Vector2::perpendicular(const int& a_c_riDirection)
{
	Vector2 temp;

	if (a_c_riDirection >= 0)
	{
		temp.fx = fy;
		temp.fy = -fx;
	}
	else
	{
		temp.fx = -fy;
		temp.fy = fx;
	}

	return temp;
}

//This function allows for the return of the magnitude of a Vector2 object
float Vector2::magnitude()
{
	return sqrtf((fx*fx) + (fy * fy));
}

//This function normalises the Vector2 it is called on
void Vector2::normalise()
{
	float fNorm = magnitude();
	fx /= fNorm;
	fy /= fNorm;
}

//This function returns a normalised Vector2 object from the calling object
Vector2 Vector2::unitVector()
{
	Vector2 temp = *this;
	float fNorm = magnitude();
	temp.fx /= fNorm;
	temp.fy /= fNorm;
	return temp;
}

//This function rotates the Vector2 object called on by a number of degrees
void Vector2::rotate(const float& a_c_rfDegrees)
{

	float fRad = -a_c_rfDegrees;
	DegreesToRadians(fRad);

	float px = 0;
	float py = 0;

	float si = sin(fRad);
	float co = cos(fRad);

	px = (fx * co) - (fy * si);
	py = (fx * si) + (fy * co);

	fx = px;
	fy = py;

}

//Operator Overloads - Mathematical
Vector2 Vector2::operator+(const float& a_c_rfScalar)
{
	return Vector2(fx + a_c_rfScalar, fy + a_c_rfScalar);
}

Vector2 Vector2::operator+(const Vector2& a_c_rVector2)
{
	return Vector2(fx + a_c_rVector2.fx, fy + a_c_rVector2.fy);
}

Vector2 Vector2::operator-(const float& a_c_rfScalar)
{
	return Vector2(fx - a_c_rfScalar, fy - a_c_rfScalar);
}

Vector2 Vector2::operator-(const Vector2& a_c_rVector2)
{
	return Vector2(fx - a_c_rVector2.fx, fy - a_c_rVector2.fy);
}

Vector2 Vector2::operator*(const float& a_c_rfScalar)
{
	return Vector2(fx * a_c_rfScalar, fy * a_c_rfScalar);
}

Vector2 Vector2::operator*(const Vector2& a_c_rVector2)
{
	return Vector2(fx * a_c_rVector2.fx, fy * a_c_rVector2.fy);
}


void Vector2::operator+=(const float& a_c_rfScalar)
{
	fx += a_c_rfScalar;
	fy += a_c_rfScalar;
}

void Vector2::operator+=(const Vector2& a_c_rVector2)
{
	fx += a_c_rVector2.fx;
	fy += a_c_rVector2.fy;
}

void Vector2::operator-=(const float& a_c_rfScalar)
{
	fx -= a_c_rfScalar;
	fy -= a_c_rfScalar;
}

void Vector2::operator-=(const Vector2& a_c_rVector2)
{
	fx -= a_c_rVector2.fx;
	fy -= a_c_rVector2.fy;
}

void Vector2::operator*=(const float& a_c_rfScalar)
{
	fx *= a_c_rfScalar;
	fy *= a_c_rfScalar;
}

void Vector2::operator*=(const Vector2& a_c_rVector2)
{
	fx *= a_c_rVector2.fx;
	fy *= a_c_rVector2.fy;
}

//Operator Overlaods - Comparison
bool Vector2::operator==(const Vector2& a_c_rVector2)
{
	if (fx == a_c_rVector2.fx && fy == a_c_rVector2.fy)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Vector2::operator!=(const Vector2& a_c_rVector2)
{
	if (fx == a_c_rVector2.fx && fy == a_c_rVector2.fy)
	{
		return false;
	}
	else
	{
		return true;
	}
}


Vector2::operator float *()
{
	return static_cast<float*>(&fx);
}

Vector2::operator const float *()
{
	return const_cast<float*>(&fx);
}