//////////////////////////////////////
//File:		Vector4.ccp				
//Author:	Matt Griffin				
//Brief:	Vector4 Code File	    
//////////////////////////////////////

#include "Vector4.h"
#include "MathsUtilities.h"
#include <cmath>

//Contructors for Vector 4
Vector4::Vector4()
{}

Vector4::Vector4(const float& a_c_rfx, const float& a_c_rfy, const float& a_c_rfz, const float& a_c_rfw) :
	fx(a_c_rfx), fy(a_c_rfy), fz(a_c_rfz), fw(a_c_rfw)
{}

Vector4::Vector4(const Vector2& a_c_rV2, const float& a_c_rfz, const float& a_c_rfw) :
	fx(a_c_rV2.fx), fy(a_c_rV2.fy), fz(a_c_rfz), fw(a_c_rfw)
{}

Vector4::Vector4(const Vector3& a_c_rV3, const float& a_c_rfw) :
	fx(a_c_rV3.fx), fy(a_c_rV3.fy), fz(a_c_rV3.fz), fw(a_c_rfw)
{}

Vector4::Vector4(const Vector4& a_c_rV4) :
	fx(a_c_rV4.fx), fy(a_c_rV4.fy), fz(a_c_rV4.fz), fw(a_c_rV4.fw)
{}

//Destructor for Vector4
Vector4::~Vector4()
{}

//Functions for Vector4

//This function returns the dot product of two objects of Vector4
float Vector4::dot(const Vector4& a_c_rV4)
{
	return (fx * a_c_rV4.fx) + (fy * a_c_rV4.fy) + (fz + a_c_rV4.fz);
}

Vector4 Vector4::cross(const Vector4& a_c_rV4)
{
	return Vector4((fy * a_c_rV4.fz) - (fz * a_c_rV4.fy), (fz * a_c_rV4.fx) - (fx * a_c_rV4.fz), (fx * a_c_rV4.fy) - (fy * a_c_rV4.fx), 0);
}

//This function allows for the return of the magnitude of a Vector4 object
float Vector4::magnitude()
{
	return sqrtf((fx * fx) + (fy * fy) + (fz * fz));
}

//This function normalises the Vector4 it is called on
void Vector4::normalise()
{
	float fNorm = magnitude();
	fx /= fNorm;
	fy /= fNorm;
	fz /= fNorm;
}

//This function returns a normalised Vector4 object from the calling object
Vector4 Vector4::unitVector()
{
	Vector4 temp = *this;
	float fNorm = magnitude();
	temp.fx /= fNorm;
	temp.fy /= fNorm;
	temp.fz /= fNorm;
	return temp;
}

//This function rotates the Vector4 object called on by a number of degrees
void Vector4::rotate(const float& a_c_rfDegreesX, const float& a_c_rfDegreesY, const float& a_c_rfDegreesZ)
{
	rotateX(a_c_rfDegreesX);
	rotateY(a_c_rfDegreesY);
	rotateZ(a_c_rfDegreesZ);
}

//This is for rotation on the X axis
void Vector4::rotateX(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	float py = 0;
	float pz = 0;

	float si = sin(fRad);
	float co = cos(fRad);

	py = (fy * co) - (fz * si);
	pz = (fy * si) + (fz * co);

	fy = py;
	fz = pz;
}

//This is for rotation on the Y axis
void Vector4::rotateY(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	float pz = 0;
	float px = 0;

	float si = sin(fRad);
	float co = cos(fRad);

	pz = (fz * co) - (fx * si);
	px = (fz * si) + (fx * co);

	fz = pz;
	fx = px;
}

//This is for rotation on the Z axis
void Vector4::rotateZ(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	float px = 0;
	float py = 0;

	float si = sin(fRad);
	float co = cos(fRad);

	px = (fx * co) - (fy * si);
	py = (fx * si) + (fy * co);

	fx = px;
	fy = py;
}

//Operator Overloads - Mathematical
Vector4 Vector4::operator+(const float& a_c_rfScalar)
{
	return Vector4(fx + a_c_rfScalar, fy + a_c_rfScalar, fz + a_c_rfScalar, 0);
}

Vector4 Vector4::operator+(const Vector4& a_c_rV4)
{
	return Vector4(fx + a_c_rV4.fx, fy + a_c_rV4.fy, fz + a_c_rV4.fz, 0);
}

Vector4 Vector4::operator-(const float& a_c_rfScalar)
{
	return Vector4(fx - a_c_rfScalar, fy - a_c_rfScalar, fz - a_c_rfScalar, 0);
}

Vector4 Vector4::operator-(const Vector4& a_c_rV4)
{
	return Vector4(fx - a_c_rV4.fx, fy - a_c_rV4.fy, fz - a_c_rV4.fz, 0);
}

Vector4 Vector4::operator*(const float& a_c_rfScalar)
{
	return Vector4(fx * a_c_rfScalar, fy * a_c_rfScalar, fz * a_c_rfScalar, 0);
}

Vector4 Vector4::operator*(const Vector4& a_c_rV4)
{
	return Vector4(fx * a_c_rV4.fx, fy * a_c_rV4.fy, fz * a_c_rV4.fz, 0);
}


void Vector4::operator+=(const float& a_c_rfScalar)
{
	fx += a_c_rfScalar;
	fy += a_c_rfScalar;
	fz += a_c_rfScalar;
}

void Vector4::operator+=(const Vector4& a_c_rV4)
{
	fx += a_c_rV4.fx;
	fy += a_c_rV4.fy;
	fz += a_c_rV4.fz;
}

void Vector4::operator-=(const float& a_c_rfScalar)
{
	fx -= a_c_rfScalar;
	fy -= a_c_rfScalar;
	fz -= a_c_rfScalar;
}

void Vector4::operator-=(const Vector4& a_c_rV4)
{
	fx -= a_c_rV4.fx;
	fy -= a_c_rV4.fy;
	fz -= a_c_rV4.fz;
}

void Vector4::operator*=(const float& a_c_rfScalar)
{
	fx *= a_c_rfScalar;
	fy *= a_c_rfScalar;
	fz *= a_c_rfScalar;
}

void Vector4::operator*=(const Vector4& a_c_rV4)
{
	fx *= a_c_rV4.fx;
	fy *= a_c_rV4.fy;
	fz *= a_c_rV4.fz;
}

//Operator Overloads - Comparison
bool Vector4::operator==(const Vector4& a_c_rV4)
{
	if (fx == a_c_rV4.fx && fy == a_c_rV4.fy && fz == a_c_rV4.fz)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Vector4::operator!=(const Vector4& a_c_rV4)
{
	if (fx == a_c_rV4.fx && fy == a_c_rV4.fy && fz == a_c_rV4.fz)
	{
		return false;
	}
	else
	{
		return true;
	}
}


Vector4::operator float *()
{
	return static_cast<float*>(&fx);
}

Vector4::operator const float *()
{
	return const_cast<float*>(&fx);
}