//////////////////////////////////////
//File:		Vector4.h				
//Author:	Matt Griffin					
//Brief:	Vector4 Header File	    
//////////////////////////////////////

#ifndef _VECTOR4_H_
#define _VECTOR4_H_

#include "Vector2.h"
#include "Vector3.h"

class Vector4
{
public:
	//Constructors for Vector4
	Vector4();
	Vector4(const float& a_c_rfx, const float& a_c_rfy, const float& a_c_rfz, const float& a_c_rfw = 0);
	Vector4(const Vector2& a_c_rV2, const float& a_c_rfz, const float& a_c_rfw = 0);
	Vector4(const Vector3& a_c_rV3, const float& a_c_rfw = 0);
	Vector4(const Vector4& a_c_rV4);

	//Destructor for Vector4
	~Vector4();

	//Functions for Vector 4
	float dot(const Vector4& a_c_rV4);
	Vector4 cross(const Vector4& a_c_rV4);

	float magnitude();

	void normalise();
	Vector4 unitVector();

	void rotate(const float& a_c_rfDegreesX, const float& a_c_rfDegreesY, const float& a_c_rfDegreesZ);

	void rotateX(const float& a_c_rfDegrees);
	void rotateY(const float& a_c_rfDegrees);
	void rotateZ(const float& a_c_rfDegrees);

	//Operator Overloads - Mathematical
	Vector4 operator+(const float& a_c_rfScalar);
	Vector4 operator+(const Vector4& a_c_rV4);

	Vector4 operator-(const float& a_c_rfScalar);
	Vector4 operator-(const Vector4& a_c_rV4);

	Vector4 operator*(const float& a_c_rfScalar);
	Vector4 operator*(const Vector4& a_c_rV4);


	void operator+=(const float& a_c_rfScalar);
	void operator+=(const Vector4& a_c_rV4);

	void operator-=(const float& a_c_rfScalar);
	void operator-=(const Vector4& a_c_rV4);

	void operator*=(const float& a_c_rfScalar);
	void operator*=(const Vector4& a_c_rV4);

	//Operator Overloads - Comparison
	bool operator==(const Vector4& a_c_rV4);
	bool operator!=(const Vector4& a_c_rV4);

	operator float *();
	operator const float *();

	//Variables for Vector4
	float fx;
	float fy;
	float fz;
	float fw;

};

#endif