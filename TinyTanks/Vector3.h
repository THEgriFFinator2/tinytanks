//////////////////////////////////////
//File:		Vector3.h				
//Author:	Matt Griffin					
//Brief:	Vector3 Header File	    
//////////////////////////////////////

#ifndef _VECTOR3_H_
#define _VECTOR3_H_

#include "Vector2.h"

class Vector3
{
public:
	//Constructors for Vector3
	Vector3();
	Vector3(const float& a_c_rfx, const float& a_c_rfy, const float& a_c_rfz);
	Vector3(const Vector2& a_c_rV2, const float& a_c_rfz = 0);
	Vector3(const Vector3& a_c_rV3);

	//Destructor for Vector3
	~Vector3();

	//Functions for Vector3
	float dot(const Vector3& a_c_rV3);
	Vector3 cross(const Vector3& a_c_rV3);

	float magnitude();

	void normalise();
	Vector3 unitVector();

	void rotate(const float& a_c_rfDegreesX, const float& a_c_rfDegreesY, const float& a_c_rfDegreesZ);

	void rotateX(const float& a_c_rfDegrees);
	void rotateY(const float& a_c_rfDegrees);
	void rotateZ(const float& a_c_rfDegrees);

	//Operator Overloads - Mathematical
	Vector3 operator+(const float& a_c_rfScalar);
	Vector3 operator+(const Vector3& a_c_rV3);

	Vector3 operator-(const float& a_c_rfScalar);
	Vector3 operator-(const Vector3& a_c_rV3);

	Vector3 operator*(const float& a_c_rfScalar);
	Vector3 operator*(const Vector3& a_c_rV3);


	void operator+=(const float& a_c_rfScalar);
	void operator+=(const Vector3& a_c_rV3);

	void operator-=(const float& a_c_rfScalar);
	void operator-=(const Vector3& a_c_rV3);

	void operator*=(const float& a_c_rfScalar);
	void operator*=(const Vector3& a_c_rV3);

	//Operator Overlaods - Comparison
	bool operator==(const Vector3& a_c_rV3);
	bool operator!=(const Vector3& a_c_rV3);

	//Variables for Vector3
	float fx;
	float fy;
	float fz;

	operator float *();
	operator const float *();

	static const Vector3 ZERO;
	static const Vector3 AXIS_Y;
	static const Vector3 IDENTITY;

};

#endif