//////////////////////////////////////
//File:		Matrix3.h				
//Author:	Matt Griffin				
//Brief:	Matrix3 Header File	    
//////////////////////////////////////

#ifndef _MATRIX_3_
#define _MATRIX_3_

#include "Vector3.h"
#include "Vector4.h"

class Matrix3
{

public:
	//Contructors for Matrix3
	Matrix3();
	Matrix3(const float& a_c_rfm11, const float& a_c_rfm12, const float& a_c_rfm13, const float& a_c_rfm21, const float& a_c_rfm22, const float& a_c_rfm23, const float& a_c_rfm31, const float& a_c_rfm32, const float& a_c_rfm33);
	Matrix3(const Vector3& a_c_rV3_1, const Vector3& a_c_rV3_2, const Vector3& a_c_rV3_3);
	Matrix3(const Matrix3& a_c_rM3);

	//Destructors for Matrix3
	~Matrix3();

	//Functions for Matrix 3
	void invert();

	void scaleX(const float& a_c_rfScalar);
	void scaleY(const float& a_c_rfScalar);
	void scaleZ(const float& a_c_rfScalar);
	void scale(const float& a_c_rfScalar);

	void transpose();

	void setToIdentity();
	void setToZero();

	Vector3 GetRow(const int& a_c_riRow, Vector3& a_c_riRow2);
	void SetRow(const int& a_c_riRow, const Vector3& a_c_rv4Vector);

	void rotate(const float& a_c_rfDegreesX, const float& a_c_rfDegreesY, const float& a_c_rfDegreesZ);

	void rotateX(const float& a_c_rfDegrees);
	void rotateY(const float& a_c_rfDegrees);
	void rotateZ(const float& a_c_rfDegrees);

	//Operator Overloads - Mathematical
	Matrix3 operator+(const float& a_c_rfScalar);
	Matrix3 operator+(const Vector3& a_c_rV3);
	Matrix3 operator+(const Vector4& a_c_rV4);
	Matrix3 operator+(const Matrix3& a_c_rM3);

	Matrix3 operator-(const float& a_c_rfScalar);
	Matrix3 operator-(const Vector3& a_c_rV3);
	Matrix3 operator-(const Vector4& a_c_rV4);
	Matrix3 operator-(const Matrix3& a_c_rM3);

	Matrix3 operator*(const float& a_c_rfScalar);
	Matrix3 operator*(const Vector3& a_c_rV3);
	Matrix3 operator*(const Vector4& a_c_rV4);
	Matrix3 operator*(const Matrix3& a_c_rM3);


	void operator+=(const float& a_c_rfScalar);
	void operator+=(const Vector3& a_c_rV3);
	void operator+=(const Vector4& a_c_rV4);
	void operator+=(const Matrix3& a_c_rM3);

	void operator-=(const float& a_c_rfScalar);
	void operator-=(const Vector3& a_c_rV3);
	void operator-=(const Vector4& a_c_rV4);
	void operator-=(const Matrix3& a_c_rM3);

	void operator*=(const float& a_c_rfScalar);
	void operator*=(const Vector3& a_c_rV3);
	void operator*=(const Vector4& a_c_rV4);
	void operator*=(const Matrix3& a_c_rM3);

	//Variables for Matrix3
	union
	{
		float m[3][3];

		struct
		{
			float m_11, m_12, m_13;
			float m_21, m_22, m_23;
			float m_31, m_32, m_33;
		};

		struct
		{
			float i[9];
		};

		struct
		{
			Vector3 xAxis;
			Vector3 yAxis;
			union
			{
				Vector3 zAxis;
				Vector3 translation;
			};
		};

	};

	static const Matrix3 ZERO;
	static const Matrix3 IDENTITY;

};

#endif