//////////////////////////////////////
//File:		Matrix4.h				
//Author:	Matt Griffin					
//Brief:	Matrix4 Header File	    
//////////////////////////////////////

#ifndef _MATRIX_4_
#define _MATRIX_4_

#include "Vector3.h"
#include "Vector4.h"
#include "Matrix3.h"

class Matrix4
{

public:
	//Contructors for Matrix4
	Matrix4();
	Matrix4(const float& a_c_rfm11, const float& a_c_rfm12, const float& a_c_rfm13, const float& a_c_rfm14, const float& a_c_rfm21, const float& a_c_rfm22, const float& a_c_rfm23, const float& a_c_rfm24, const float& a_c_rfm31, const float& a_c_rfm32, const float& a_c_rfm33, const float& a_c_rfm34, const float& a_c_rfm41, const float& a_c_rfm42, const float& a_c_rfm43, const float& a_c_rfm44);
	Matrix4(const Vector4& a_c_rV4_1, const Vector4& a_c_rV4_2, const Vector4& a_c_rV4_3, const Vector4& a_c_rV4_4);
	Matrix4(const Matrix4& a_c_rM4);

	//Destructors for Matrix4
	~Matrix4();


	//Functions for Matrix4
	bool invert();
	float determinant3();
	float Recipf(float a_fDet);

	void scaleX(const float& a_c_rfScalar);
	void scaleY(const float& a_c_rfScalar);
	void scaleZ(const float& a_c_rfScalar);
	void scale(const float& a_c_rfScalar);
	void scale(const float& a_c_rfScalarX, const float& a_c_rfScalarY, const float& a_c_rfScalarZ);

	void transpose();

	void setToIdentity();
	void setToZero();

	void rotate(const float& a_c_rfDegreesX, const float& a_c_rfDegreesY, const float& a_c_rfDegreesZ);

	void rotateX(const float& a_c_rfDegrees);
	void rotateY(const float& a_c_rfDegrees);
	void rotateZ(const float& a_c_rfDegrees);

	//Operator Overloads - Mathematical
	Matrix4 operator+(const float& a_c_rfScalar);
	Matrix4 operator+(const Vector3& a_c_rV3);
	Matrix4 operator+(const Vector4& a_c_rV4);
	Matrix4 operator+(const Matrix4& a_c_rM4);

	Matrix4 operator-(const float& a_c_rfScalar);
	Matrix4 operator-(const Vector3& a_c_rV3);
	Matrix4 operator-(const Vector4& a_c_rV4);
	Matrix4 operator-(const Matrix4& a_c_rM4);

	Matrix4 operator*(const float& a_c_rfScalar);
	Matrix4 operator*(const Vector3& a_c_rV3);
	Matrix4 operator*(const Vector4& a_c_rV4);
	Matrix4 operator*(const Matrix4& a_c_rM4);


	void operator+=(const float& a_c_rfScalar);
	void operator+=(const Vector3& a_c_rV3);
	void operator+=(const Vector4& a_c_rV4);
	void operator+=(const Matrix4& a_c_rM4);

	void operator-=(const float& a_c_rfScalar);
	void operator-=(const Vector3& a_c_rV3);
	void operator-=(const Vector4& a_c_rV4);
	void operator-=(const Matrix4& a_c_rM4);

	void operator*=(const float& a_c_rfScalar);
	void operator*=(const Vector3& a_c_rV3);
	void operator*=(const Vector4& a_c_rV4);
	void operator*=(const Matrix4& a_c_rM4);

	//Variables for Matrix4
	union
	{
		float m[4][4];

		struct
		{
			float m_11, m_12, m_13, m_14;
			float m_21, m_22, m_23, m_24;
			float m_31, m_32, m_33, m_34;
			float m_41, m_42, m_43, m_44;
		};

		struct
		{
			float i[16];
		};

		struct
		{
			Vector4 xAxis;
			Vector4 yAxis;
			Vector4 zAxis;
			union
			{
				Vector4 wAxis;
				Vector4 translation;
			};
		};

	};
	//Zero and Identiy
	static const Matrix4 ZERO;
	static const Matrix4 IDENTITY;
};

#endif