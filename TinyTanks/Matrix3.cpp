//////////////////////////////////////
//File:		Matrix3.ccp				
//Author:	Matt Griffin				
//Brief:	Matrix3 Code File	    
//////////////////////////////////////

#include "Matrix3.h"
#include "MathsUtilities.h"
#include <cmath>

//Contructors for Matrix3
Matrix3::Matrix3()
{}

Matrix3::Matrix3(const float& a_c_rfm11, const float& a_c_rfm12, const float& a_c_rfm13, const float& a_c_rfm21, const float& a_c_rfm22, const float& a_c_rfm23, const float& a_c_rfm31, const float& a_c_rfm32, const float& a_c_rfm33)
{
	m_11 = a_c_rfm11;
	m_12 = a_c_rfm12;
	m_13 = a_c_rfm13;

	m_21 = a_c_rfm21;
	m_22 = a_c_rfm22;
	m_23 = a_c_rfm23;

	m_31 = a_c_rfm31;
	m_32 = a_c_rfm32;
	m_33 = a_c_rfm33;
}

Matrix3::Matrix3(const Vector3& a_c_rV3_1, const Vector3& a_c_rV3_2, const Vector3& a_c_rV3_3)
{
	xAxis.fx = a_c_rV3_1.fx;
	xAxis.fy = a_c_rV3_1.fy;
	xAxis.fz = a_c_rV3_1.fz;

	yAxis.fx = a_c_rV3_2.fx;
	yAxis.fy = a_c_rV3_2.fy;
	yAxis.fz = a_c_rV3_2.fz;

	zAxis.fx = a_c_rV3_3.fx;
	zAxis.fy = a_c_rV3_3.fy;
	zAxis.fz = a_c_rV3_3.fz;
}

Matrix3::Matrix3(const Matrix3& a_c_rM3)
{
	m_11 = a_c_rM3.m_11;
	m_12 = a_c_rM3.m_12;
	m_13 = a_c_rM3.m_13;

	m_21 = a_c_rM3.m_21;
	m_22 = a_c_rM3.m_22;
	m_23 = a_c_rM3.m_23;

	m_31 = a_c_rM3.m_31;
	m_32 = a_c_rM3.m_32;
	m_33 = a_c_rM3.m_33;
}

//Destructor for Matrix3
Matrix3::~Matrix3()
{}

//Functions for Matrix3
void Matrix3::invert()
{
	Matrix3 temp;

	// create a Matrix of Minors
	temp.m_11 = (m_22 * m_33) - (m_23 * m_32);
	temp.m_12 = (m_21 * m_33) - (m_23 * m_31);
	temp.m_13 = (m_21 * m_32) - (m_22 * m_31);

	temp.m_21 = (m_12 * m_33) - (m_13 * m_32);
	temp.m_22 = (m_11 * m_33) - (m_13 * m_31);
	temp.m_23 = (m_11 * m_32) - (m_12 * m_31);

	temp.m_31 = (m_12 * m_23) - (m_13 * m_22);
	temp.m_32 = (m_11 * m_23) - (m_13 * m_21);
	temp.m_33 = (m_11 * m_22) - (m_12 * m_21);

	// find the determinant

	float determinant = (m_11 * temp.m_11) - (m_12 * temp.m_12) + (m_13 * temp.m_13);

	// create a Matrix of Cofactors

	temp.m_12 = -temp.m_12;
	temp.m_21 = -temp.m_21;
	temp.m_23 = -temp.m_23;
	temp.m_32 = -temp.m_32;

	// and then Adjugate or Adjoint

	temp.transpose();

	// finally calculate the temp

	*this = temp * (1 / determinant);

}
void Matrix3::scaleX(const float& a_c_rfScalar)
{
	xAxis *= a_c_rfScalar;
}

void Matrix3::scaleY(const float& a_c_rfScalar)
{
	yAxis *= a_c_rfScalar;
}

void Matrix3::scaleZ(const float& a_c_rfScalar)
{
	zAxis *= a_c_rfScalar;
}

void Matrix3::scale(const float& a_c_rfScalar)
{
	scaleX(a_c_rfScalar);
	scaleY(a_c_rfScalar);
	scaleZ(a_c_rfScalar);
}

void Matrix3::transpose()
{
	float fTempm_12 = m_21, fTempm_13 = m_31;
	float fTempm_23 = m_32;

	float fTempm_21 = m_12;
	float fTempm_31 = m_13, fTempm_32 = m_23;

	m_12 = fTempm_12;
	m_13 = fTempm_13;
	m_23 = fTempm_23;

	m_21 = fTempm_21;
	m_31 = fTempm_31;
	m_32 = fTempm_32;
}

void Matrix3::setToIdentity()
{
	m_11 = 1;
	m_12 = 0;
	m_13 = 0;

	m_21 = 0;
	m_22 = 1;
	m_23 = 0;

	m_31 = 0;
	m_32 = 0;
	m_33 = 1;
}

void Matrix3::setToZero()
{
	m_11 = 0;
	m_12 = 0;
	m_13 = 0;

	m_21 = 0;
	m_22 = 0;
	m_23 = 0;

	m_31 = 0;
	m_32 = 0;
	m_33 = 0;
}

void Matrix3::rotateX(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	const float si = sinf(fRad);
	const float co = cosf(fRad);
	m_11 = 1.0f; m_12 = 0.0f; m_13 = 0.0f;
	m_21 = 0.0f; m_22 = co; m_23 = si;
	m_31 = 0.0f; m_32 = -si; m_33 = co;
}

void Matrix3::rotateY(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	const float si = sinf(fRad);
	const float co = cosf(fRad);
	m_11 = co; m_12 = 0.0f; m_13 = -si;
	m_21 = 0.0f; m_22 = 1.0f; m_23 = 0.0f;
	m_31 = si; m_32 = 0.0f; m_33 = co;
}

void Matrix3::rotateZ(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	const float si = sinf(fRad);
	const float co = cosf(fRad);
	m_11 = co; m_12 = si; m_13 = 0.0f;
	m_21 = -si; m_22 = co; m_23 = 0.0f;
	m_31 = 0.0f; m_32 = 0.0f; m_33 = 1.0f;
}

Vector3 Matrix3::GetRow(const int& a_c_riRow, Vector3& a_c_riRow2)
{
	switch (a_c_riRow)
	{
	case 1:
	{
		return xAxis;
		break;
	}
	case 2:
	{
		return yAxis;
		break;
	}
	case 3:
	{
		return zAxis;
		break;
	}
	default:
	{
		return Vector3(0.f, 0.f, 0.f);
		break;
	}
	}
}


void Matrix3::SetRow(const int& a_c_riRow, const Vector3& a_c_rv4Vector)
{
	switch (a_c_riRow)
	{
	case 1:
	{
		xAxis = a_c_rv4Vector;
		break;
	}
	case 2:
	{
		yAxis = a_c_rv4Vector;
			break;
	}
	case 3:
	{
		zAxis = a_c_rv4Vector;
		break;
	}
	default:
	{
		break;
	}
	}
}

//Operator Overloads - Mathematical
Matrix3 Matrix3::operator+(const float& a_c_rfScalar)
{
	Matrix3 temp = *this;

	temp.m_11 += a_c_rfScalar;
	temp.m_12 += a_c_rfScalar;
	temp.m_13 += a_c_rfScalar;

	temp.m_21 += a_c_rfScalar;
	temp.m_22 += a_c_rfScalar;
	temp.m_23 += a_c_rfScalar;

	temp.m_31 += a_c_rfScalar;
	temp.m_32 += a_c_rfScalar;
	temp.m_33 += a_c_rfScalar;

	return temp;
}

Matrix3 Matrix3::operator+(const Vector3& a_c_rV3)
{
	Matrix3 temp = *this;



	return temp;
}

Matrix3 Matrix3::operator+(const Vector4& a_c_rV4)
{
	Matrix3 temp = *this;



	return temp;
}

Matrix3 Matrix3::operator+(const Matrix3& a_c_rM3)
{
	Matrix3 temp = *this;

	temp.m_11 += a_c_rM3.m_11;
	temp.m_12 += a_c_rM3.m_12;
	temp.m_13 += a_c_rM3.m_13;

	temp.m_21 += a_c_rM3.m_21;
	temp.m_22 += a_c_rM3.m_22;
	temp.m_23 += a_c_rM3.m_23;

	temp.m_31 += a_c_rM3.m_31;
	temp.m_32 += a_c_rM3.m_32;
	temp.m_33 += a_c_rM3.m_33;

	return temp;
}

Matrix3 Matrix3::operator-(const float& a_c_rfScalar)
{
	Matrix3 temp = *this;

	temp.m_11 -= a_c_rfScalar;
	temp.m_12 -= a_c_rfScalar;
	temp.m_13 -= a_c_rfScalar;

	temp.m_21 -= a_c_rfScalar;
	temp.m_22 -= a_c_rfScalar;
	temp.m_23 -= a_c_rfScalar;

	temp.m_31 -= a_c_rfScalar;
	temp.m_32 -= a_c_rfScalar;
	temp.m_33 -= a_c_rfScalar;

	return temp;
}

Matrix3 Matrix3::operator-(const Matrix3& a_c_rM3)
{
	Matrix3 temp = *this;

	temp.m_11 -= a_c_rM3.m_11;
	temp.m_12 -= a_c_rM3.m_12;
	temp.m_13 -= a_c_rM3.m_13;

	temp.m_21 -= a_c_rM3.m_21;
	temp.m_22 -= a_c_rM3.m_22;
	temp.m_23 -= a_c_rM3.m_23;

	temp.m_31 -= a_c_rM3.m_31;
	temp.m_32 -= a_c_rM3.m_32;
	temp.m_33 -= a_c_rM3.m_33;

	return temp;
}

Matrix3 Matrix3::operator*(const float& a_c_rfScalar)
{
	Matrix3 temp = *this;

	temp.m_11 *= a_c_rfScalar;
	temp.m_12 *= a_c_rfScalar;
	temp.m_13 *= a_c_rfScalar;

	temp.m_21 *= a_c_rfScalar;
	temp.m_22 *= a_c_rfScalar;
	temp.m_23 *= a_c_rfScalar;

	temp.m_31 *= a_c_rfScalar;
	temp.m_32 *= a_c_rfScalar;
	temp.m_33 *= a_c_rfScalar;

	return temp;
}

Matrix3 Matrix3::operator*(const Matrix3& a_c_rM3)
{
	Matrix3 temp = *this;

	temp.m_11 *= a_c_rM3.m_11;
	temp.m_12 *= a_c_rM3.m_12;
	temp.m_13 *= a_c_rM3.m_13;

	temp.m_21 *= a_c_rM3.m_21;
	temp.m_22 *= a_c_rM3.m_22;
	temp.m_23 *= a_c_rM3.m_23;

	temp.m_31 *= a_c_rM3.m_31;
	temp.m_32 *= a_c_rM3.m_32;
	temp.m_33 *= a_c_rM3.m_33;

	return temp;
}


void Matrix3::operator+=(const float& a_c_rfScalar)
{
	m_11 += a_c_rfScalar;
	m_12 += a_c_rfScalar;
	m_13 += a_c_rfScalar;

	m_21 += a_c_rfScalar;
	m_22 += a_c_rfScalar;
	m_23 += a_c_rfScalar;

	m_31 += a_c_rfScalar;
	m_32 += a_c_rfScalar;
	m_33 += a_c_rfScalar;
}

void Matrix3::operator+=(const Matrix3& a_c_rM3)
{
	m_11 += a_c_rM3.m_11;
	m_12 += a_c_rM3.m_12;
	m_13 += a_c_rM3.m_13;

	m_21 += a_c_rM3.m_21;
	m_22 += a_c_rM3.m_22;
	m_23 += a_c_rM3.m_23;

	m_31 += a_c_rM3.m_31;
	m_32 += a_c_rM3.m_32;
	m_33 += a_c_rM3.m_33;
}

void Matrix3::operator-=(const float& a_c_rfScalar)
{
	m_11 -= a_c_rfScalar;
	m_12 -= a_c_rfScalar;
	m_13 -= a_c_rfScalar;

	m_21 -= a_c_rfScalar;
	m_22 -= a_c_rfScalar;
	m_23 -= a_c_rfScalar;

	m_31 -= a_c_rfScalar;
	m_32 -= a_c_rfScalar;
	m_33 -= a_c_rfScalar;
}

void Matrix3::operator-=(const Matrix3& a_c_rM3)
{
	m_11 -= a_c_rM3.m_11;
	m_12 -= a_c_rM3.m_12;
	m_13 -= a_c_rM3.m_13;

	m_21 -= a_c_rM3.m_21;
	m_22 -= a_c_rM3.m_22;
	m_23 -= a_c_rM3.m_23;

	m_31 -= a_c_rM3.m_31;
	m_32 -= a_c_rM3.m_32;
	m_33 -= a_c_rM3.m_33;
}

void Matrix3::operator*=(const float& a_c_rfScalar)
{
	m_11 *= a_c_rfScalar;
	m_12 *= a_c_rfScalar;
	m_13 *= a_c_rfScalar;

	m_21 *= a_c_rfScalar;
	m_22 *= a_c_rfScalar;
	m_23 *= a_c_rfScalar;

	m_31 *= a_c_rfScalar;
	m_32 *= a_c_rfScalar;
	m_33 *= a_c_rfScalar;
}

void Matrix3::operator*=(const Matrix3& a_c_rM3)
{
	m_11 *= a_c_rM3.m_11;
	m_12 *= a_c_rM3.m_12;
	m_13 *= a_c_rM3.m_13;

	m_21 *= a_c_rM3.m_21;
	m_22 *= a_c_rM3.m_22;
	m_23 *= a_c_rM3.m_23;

	m_31 *= a_c_rM3.m_31;
	m_32 *= a_c_rM3.m_32;
	m_33 *= a_c_rM3.m_33;
}

const Matrix3 Matrix3::ZERO(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
const Matrix3 Matrix3::IDENTITY(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
