//////////////////////////////////////
//File:		Vector3.ccp				
//Author:	Matt Griffin					
//Brief:	Vector3 Code File	    
//////////////////////////////////////

#include "Vector3.h"
#include "Matrix3.h"
#include "MathsUtilities.h"
#include <cmath>

//Constructors for Vector3
Vector3::Vector3()
{}

Vector3::Vector3(const float& a_c_rfx, const float& a_c_rfy, const float& a_c_rfz) :
	fx(a_c_rfx), fy(a_c_rfy), fz(a_c_rfz)
{}

Vector3::Vector3(const Vector2& a_c_rV2, const float& a_c_rfz) :
	fx(a_c_rV2.fx), fy(a_c_rV2.fy), fz(a_c_rfz)
{}

Vector3::Vector3(const Vector3& a_c_rV3) :
	fx(a_c_rV3.fx), fy(a_c_rV3.fy), fz(a_c_rV3.fz)
{}

//Destructor for Vector3
Vector3::~Vector3()
{}

//Functions for Vector3

//This function returns the dot product of two objects of Vector3
float Vector3::dot(const Vector3& a_c_rV3)
{
	return (fx * a_c_rV3.fx) + (fy * a_c_rV3.fy) + (fz + a_c_rV3.fz);
}

Vector3 Vector3::cross(const Vector3& a_c_rV3)
{
	return Vector3((fy * a_c_rV3.fz) - (fz * a_c_rV3.fy), (fz * a_c_rV3.fx) - (fx * a_c_rV3.fz), (fx * a_c_rV3.fy) - (fy * a_c_rV3.fx));
}

//This function allows for the return of the magnitude of a Vector3 object
float Vector3::magnitude()
{
	return sqrtf((fx * fx) + (fy * fy) + (fz * fz));
}

//This function normalises the Vector3 it is called on
void Vector3::normalise()
{
	float fNorm = magnitude();
	fx /= fNorm;
	fy /= fNorm;
	fz /= fNorm;
}

//This function returns a normalised Vector3 object from the calling object
Vector3 Vector3::unitVector()
{
	Vector3 temp = *this;
	float fNorm = magnitude();
	temp.fx /= fNorm;
	temp.fy /= fNorm;
	temp.fz /= fNorm;
	return temp;
}

//This function rotates the Vector3 object called on by a number of degrees
void Vector3::rotate(const float& a_c_rfDegreesX, const float& a_c_rfDegreesY, const float& a_c_rfDegreesZ)
{
	rotateX(a_c_rfDegreesX);
	rotateY(a_c_rfDegreesY);
	rotateZ(a_c_rfDegreesZ);
}

//This is for rotation on the X axis
void Vector3::rotateX(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	float py = 0;
	float pz = 0;

	float si = sinf(fRad);
	float co = cosf(fRad);

	py = (fy * co) - (fz * si);
	pz = (fy * si) + (fz * co);

	fy = py;
	fz = pz;
}

//This is for rotation on the Y axis
void Vector3::rotateY(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	float pz = 0;
	float px = 0;

	float si = sinf(fRad);
	float co = cosf(fRad);

	pz = (fz * co) - (fx * si);
	px = (fz * si) + (fx * co);

	fz = pz;
	fx = px;
}

//This is for rotation on the Z axis
void Vector3::rotateZ(const float& a_c_rfDegrees)
{
	float fRad = a_c_rfDegrees;
	DegreesToRadians(fRad);

	float px = 0;
	float py = 0;

	float si = sinf(fRad);
	float co = cosf(fRad);

	px = (fx * co) - (fy * si);
	py = (fx * si) + (fy * co);

	fx = px;
	fy = py;
}

//Operator Overloads - Mathematical
Vector3 Vector3::operator+(const float& a_c_rfScalar)
{
	return Vector3(fx + a_c_rfScalar, fy + a_c_rfScalar, fz + a_c_rfScalar);
}

Vector3 Vector3::operator+(const Vector3& a_c_rV3)
{
	return Vector3(fx + a_c_rV3.fx, fy + a_c_rV3.fy, fz + a_c_rV3.fz);
}

Vector3 Vector3::operator-(const float& a_c_rfScalar)
{
	return Vector3(fx - a_c_rfScalar, fy - a_c_rfScalar, fz - a_c_rfScalar);
}

Vector3 Vector3::operator-(const Vector3& a_c_rV3)
{
	return Vector3(fx - a_c_rV3.fx, fy - a_c_rV3.fy, fz - a_c_rV3.fz);
}

Vector3 Vector3::operator*(const float& a_c_rfScalar)
{
	return Vector3(fx * a_c_rfScalar, fy * a_c_rfScalar, fz * a_c_rfScalar);
}

Vector3 Vector3::operator*(const Vector3& a_c_rV3)
{
	return Vector3(fx * a_c_rV3.fx, fy * a_c_rV3.fy, fz * a_c_rV3.fz);
}


void Vector3::operator+=(const float& a_c_rfScalar)
{
	fx += a_c_rfScalar;
	fy += a_c_rfScalar;
	fz += a_c_rfScalar;
}

void Vector3::operator+=(const Vector3& a_c_rV3)
{
	fx += a_c_rV3.fx;
	fy += a_c_rV3.fy;
	fz += a_c_rV3.fz;
}

void Vector3::operator-=(const float& a_c_rfScalar)
{
	fx -= a_c_rfScalar;
	fy -= a_c_rfScalar;
	fz -= a_c_rfScalar;
}

void Vector3::operator-=(const Vector3& a_c_rV3)
{
	fx -= a_c_rV3.fx;
	fy -= a_c_rV3.fy;
	fz -= a_c_rV3.fz;
}

void Vector3::operator*=(const float& a_c_rfScalar)
{
	fx *= a_c_rfScalar;
	fy *= a_c_rfScalar;
	fz *= a_c_rfScalar;
}

void Vector3::operator*=(const Vector3& a_c_rV3)
{
	fx *= a_c_rV3.fx;
	fy *= a_c_rV3.fy;
	fz *= a_c_rV3.fz;
}

//Operator Overloads - Comparison
bool Vector3::operator==(const Vector3& a_c_rV3)
{
	if (fx == a_c_rV3.fx && fy == a_c_rV3.fy && a_c_rV3.fz)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Vector3::operator!=(const Vector3& a_c_rV3)
{
	if (fx == a_c_rV3.fx && fy == a_c_rV3.fy && a_c_rV3.fz)
	{
		return false;
	}
	else
	{
		return true;
	}
}

Vector3::operator float *()
{
	return static_cast<float*>(&fx);
}

Vector3::operator const float *()
{
	return const_cast<float*>(&fx);
}
