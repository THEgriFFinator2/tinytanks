//////////////////////////////////////
//File:		MathsUltilities.ccp		
//Author:	Matt Griffin				
//Brief:	Maths Libary Code	    
//////////////////////////////////////


#include "MathsUtilities.h"

//Values used in calculations
#define PI			3.1415926536f
#define PIHALF		1.5707963268f
#define PIQUARTER	0.7853981625f

void DegreesToRadians(float& a_rfDegrees)
{
	a_rfDegrees *= (PI / 180.f);
}

void RadiansToDegrees(float& a_rfRadians)
{
	a_rfRadians *= (180.f / PI);
}