//////////////////////////////////////
//File:		Vector2.h				
//Author:	Matt Griffin				
//Brief:	Vector2 Header File	    
//////////////////////////////////////

#ifndef _VECTOR2_H_
#define _VECTOR2_H_

class Vector2
{
public:
	//Constructors for Vector 2	
	Vector2();
	Vector2(const float& a_c_rfx, const float& a_c_rfy);
	Vector2(const Vector2& a_c_rVector2);

	//Destructor for Vector 2
	~Vector2();

	//Functions for Vector 2
	float dot(const Vector2& a_c_rVector2);

	Vector2 perpendicular(const int& a_c_riDirection = 0);

	float magnitude();

	void normalise();
	Vector2 unitVector();

	void rotate(const float& a_c_rfDegrees);

	//Operator Overloads - Mathematical 
	Vector2 operator+(const float& a_c_rfScalar);
	Vector2 operator+(const Vector2& a_c_rVector2);

	Vector2 operator-(const float& a_c_rfScalar);
	Vector2 operator-(const Vector2& a_c_rVector2);

	Vector2 operator*(const float& a_c_rfScalar);
	Vector2 operator*(const Vector2& a_c_rVector2);


	void operator+=(const float& a_c_rfScalar);
	void operator+=(const Vector2& a_c_rVector2);

	void operator-=(const float& a_c_rfScalar);
	void operator-=(const Vector2& a_c_rVector2);

	void operator*=(const float& a_c_rfScalar);
	void operator*=(const Vector2& a_c_rVector2);

	//Operator Overloads - Comparison
	bool operator==(const Vector2& a_c_rVector2);
	bool operator!=(const Vector2& a_c_rVector2);

	operator float *();
	operator const float *();

	//Variables for Vector 2
	float fx;
	float fy;


};

#endif
